﻿using System;
using System.Linq;
using SchoolDbLabOne.Joske.ISD.Labs.LabOne.Model;
using SchoolDbLabOne.Joske.ISD.Labs.LabOne.repository.schoolrepository;
using SchoolDbLabOne.Joske.ISD.Labs.LabOne.Repository.ClassRepository;
using SchoolDbLabOne.Joske.ISD.Labs.LabOne.Repository.CourseRepository;
using SchoolDbLabOne.Joske.ISD.Labs.LabOne.Repository.ScheduleRepository;
using SchoolDbLabOne.Joske.ISD.Labs.LabOne.Repository.TeacherRepository;
using Unity.Interception.Utilities;

namespace SchoolDbLabOne
{
    class Program
    {
        private static SchoolDBEntities context = new SchoolDBEntities();
        private static ISchoolRepository schoolRepository = new SchoolRepository(context);
        private static ITeacherRepository teacherRepository = new TeacherRepository(context);
        private static IClassRepository classRepository = new ClassRepository(context);
        private static ICourseRepository courseRepository = new CourseRepository(context);
        private static IScheduleRepository scheduleRepository = new ScheduleRepository(context);

        static void Main(string[] args)
        {
            School school = new School();
            school.id = 1;
            school.SchoolName = "LLG";
            school.Budget = new decimal(23243445);
            school.IsHigher = true;
            Console.WriteLine(schoolRepository.Get(5).SchoolName);
            schoolRepository.Add(school);
            Console.WriteLine(schoolRepository.GetAllByBudget(new decimal(23243445)).First().SchoolName);
            scheduleRepository.GetByValidity(true).ForEach(i => Console.WriteLine(i.StartDate.Date));

            Console.WriteLine("Number of courses by Jonh Doe: {0}", teacherRepository.GetByFullNameAndSchoolId("Jonh", "Doe", 5).TeacherCourses.Count);
            Console.Read();
        }
    }

    
}
