﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolDbLabOne.Joske.ISD.Labs.LabOne.Model;
using SchoolDbLabOne.repository;

namespace SchoolDbLabOne.Joske.ISD.Labs.LabOne.repository.schoolrepository
{
    interface ISchoolRepository : IRepository<School>
    {
        School GetByName(string name);
        IEnumerable<School> GetAllByBudget(decimal budget);
    }
}
