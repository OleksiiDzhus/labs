﻿using SchoolDbLabOne.Joske.ISD.Labs.LabOne.Model;
using SchoolDbLabOne.repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolDbLabOne.Joske.ISD.Labs.LabOne.repository.schoolrepository
{
     class SchoolRepository : Repository<School>, ISchoolRepository
    {

        public SchoolRepository(DbContext context)
            : base(context)
        {

        }

        public IEnumerable<School> GetAllByBudget(decimal budget) => Context.Set<School>().Where(s => s.Budget == budget).ToList();

        public School GetByName(string name) => Context.Set<School>().FirstOrDefault(s => s.SchoolName.Equals(name));
    }
}
