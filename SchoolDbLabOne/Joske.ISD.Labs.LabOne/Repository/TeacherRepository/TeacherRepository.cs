﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolDbLabOne.Joske.ISD.Labs.LabOne.Model;
using SchoolDbLabOne.repository;

namespace SchoolDbLabOne.Joske.ISD.Labs.LabOne.Repository.TeacherRepository
{
    class TeacherRepository : Repository<Teacher>, ITeacherRepository
    {
        public TeacherRepository(DbContext context)
            : base(context)
        {

        }

        public Teacher GetByFullNameAndSchoolId(string firstName, string lastName, int schoolId) => Context
                .Set<Teacher>()
                .FirstOrDefault(t => t.FirstName.Equals(firstName)
                            && t.LastName.Equals(lastName)
                            && t.SchoolId == schoolId);

        public IEnumerable<Teacher> GetByHireDate(DateTime hireDate) =>
            Context.Set<Teacher>().Where(t => t.HireDate.Equals(hireDate)).ToList();
    }
}
