﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolDbLabOne.Joske.ISD.Labs.LabOne.Model;
using SchoolDbLabOne.repository;

namespace SchoolDbLabOne.Joske.ISD.Labs.LabOne.Repository.TeacherRepository
{
    interface ITeacherRepository : IRepository<Teacher>
    {
        Teacher GetByFullNameAndSchoolId(string firstName, string lastName, int schoolId);
        IEnumerable<Teacher> GetByHireDate(DateTime hireDate);
    }
}
