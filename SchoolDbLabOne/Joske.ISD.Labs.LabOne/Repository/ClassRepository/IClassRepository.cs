﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolDbLabOne.Joske.ISD.Labs.LabOne.Model;
using SchoolDbLabOne.repository;

namespace SchoolDbLabOne.Joske.ISD.Labs.LabOne.Repository.ClassRepository
{
    interface IClassRepository : IRepository<Class>
    {
        Class GetByClassName(string name);
        IEnumerable<Class> GetByStudentsNumber(short studentsNumber);
    }
}
