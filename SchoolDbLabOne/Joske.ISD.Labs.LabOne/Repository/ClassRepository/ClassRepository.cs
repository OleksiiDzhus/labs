﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolDbLabOne.Joske.ISD.Labs.LabOne.Model;
using SchoolDbLabOne.repository;

namespace SchoolDbLabOne.Joske.ISD.Labs.LabOne.Repository.ClassRepository
{
    class ClassRepository : Repository<Class>, IClassRepository
    {
        public ClassRepository(DbContext context) : base(context)
        {
        }

        public Class GetByClassName(string name) =>
            Context.Set<Class>().FirstOrDefault(c => c.ClassName.Equals(name));

        public IEnumerable<Class> GetByStudentsNumber(short studentsNumber) =>
            Context.Set<Class>()
                .Where(c => c.StudentsNumber == studentsNumber)
                .OrderByDescending(c => c.ClassName)
                .ToList();
    }
}
