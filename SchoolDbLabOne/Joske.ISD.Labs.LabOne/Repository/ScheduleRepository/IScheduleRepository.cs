﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolDbLabOne.Joske.ISD.Labs.LabOne.Model;
using SchoolDbLabOne.repository;

namespace SchoolDbLabOne.Joske.ISD.Labs.LabOne.Repository.ScheduleRepository
{
    interface IScheduleRepository : IRepository<Schedule>
    {
        IEnumerable<Schedule> GetByValidity(bool isValid);
        IEnumerable<Schedule> GetByStartDate(DateTime startDate);
    }
}
