﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolDbLabOne.Joske.ISD.Labs.LabOne.Model;
using SchoolDbLabOne.repository;

namespace SchoolDbLabOne.Joske.ISD.Labs.LabOne.Repository.ScheduleRepository
{
    class ScheduleRepository : Repository<Schedule>, IScheduleRepository
    {
        public ScheduleRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<Schedule> GetByStartDate(DateTime startDate) =>
            Context.Set<Schedule>().Where(s => s.StartDate.Equals(startDate))
                .OrderByDescending(s => s.StartDate)
                .ToList();

        public IEnumerable<Schedule> GetByValidity(bool isValid) =>
            Context.Set<Schedule>().Where(s => s.IsValid == isValid).ToList();
    }
}