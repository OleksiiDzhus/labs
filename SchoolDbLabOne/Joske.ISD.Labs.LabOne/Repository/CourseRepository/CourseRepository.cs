﻿using SchoolDbLabOne.Joske.ISD.Labs.LabOne.Model;
using SchoolDbLabOne.repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolDbLabOne.Joske.ISD.Labs.LabOne.Repository.CourseRepository
{
    class CourseRepository : Repository<Course>, ICourseRepository
    {
        public CourseRepository(DbContext context) : base(context)
        {
        }

        public Course GetByCourseName(string courseName) => 
            Context.Set<Course>().FirstOrDefault(c => c.CourseName.Equals(courseName));

        public IEnumerable<Course> GetByCreditsNumber(int creditsNumber) =>
            Context.Set<Course>().Where(c => c.Credits == creditsNumber).ToList();

        public IEnumerable<Course> GetTopCreditedCourses(int count) =>
            Context.Set<Course>()
                .OrderByDescending(c => c.Credits)
                .Take(count)
                .ToList();
    }
}
