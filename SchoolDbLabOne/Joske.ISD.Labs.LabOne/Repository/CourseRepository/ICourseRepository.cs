﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolDbLabOne.Joske.ISD.Labs.LabOne.Model;
using SchoolDbLabOne.repository;

namespace SchoolDbLabOne.Joske.ISD.Labs.LabOne.Repository.CourseRepository
{
    interface ICourseRepository : IRepository<Course>
    {
        Course GetByCourseName(string courseName);
        IEnumerable<Course> GetByCreditsNumber(int creditsNumber);
        IEnumerable<Course> GetTopCreditedCourses(int count);
    }
}
